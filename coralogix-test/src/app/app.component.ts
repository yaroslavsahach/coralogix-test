import { Component, OnInit } from '@angular/core';
import {Log, Severity, CoralogixLogger, LoggerConfig} from "coralogix-logger";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  generateInterval;
  private users: string[] = ['Anna', 'Bob', 'Casey', 'Dolores', 'Elon', 'Finn'];
  private sections: string[] = ['Music', 'Videos', 'Friends', 'Messages', 'Settings'];
  logs: string[] = [""];

  config = new LoggerConfig({
    applicationName: "coralogix-test",
    privateKey: "35538d7b-8df3-5ec3-767a-2c2c27b6c846",
    subsystemName: "user navigation test",
  });
  
  logger: CoralogixLogger;
  
  ngOnInit() {
    CoralogixLogger.configure(this.config);
    this.logger = new CoralogixLogger("Users actions");
  }

  getLogMessage(userId: number, sectionId: number, severity: number): string {
    var result = "";
    switch (severity) {
      case 1:
        result += this.users[userId] + " successfully navigated to section: " + this.sections[sectionId];
        break;
      case 2:
        result += "Current application users: "+ this.users.toString();
        break;
      case 3:
        result += this.users[userId] + " send message to " + this.users[(userId + 1) % 6];
        break;
      case 4:
        result += this.users[userId] + " encountered a warning while sending message to " + this.users[(userId + 1) % 6];
        break;
      case 5:
        result += this.users[userId] + " encountered an error while navigation to section: " + this.sections[sectionId];
        break;
      case 6:
        result += "Critical error! Application crashed!";
        break;
      default:
        result += "Unknown severity of log";
        break;
    }
    return result;
  }

  getRandomSeverity(): number {
    let severityProbability = Math.random(); 
    if (severityProbability < 0.25) return 1
    else if (severityProbability < 0.3) return 2
    else if (severityProbability < 0.65) return 3
    else if (severityProbability < 0.8) return 4
    else if (severityProbability < 0.95) return 5
    else return 6
  }

  onStart() {
  	this.generateInterval = setInterval(() => {
      let addLogProbability = Math.random(); // If probability < 0.3, then generate and send log
  		if (addLogProbability < 0.3) {
        let severity = this.getRandomSeverity();
        let message = this.getLogMessage(
          Math.floor(Math.random() * 6), // Random user from users list
          Math.floor(Math.random() * 5), // Random section from sections list,
          severity
        );
        let log = new Log({
          severity: severity,           
          className: "users action",
          text: message
        })
        
        this.logger.addLog(log);
        this.logs.push(message);
      }
  	}, 1000);
  }

  onStop() {
  	clearInterval(this.generateInterval);
  }
}